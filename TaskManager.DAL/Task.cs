namespace TaskManager.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Task")]
    public partial class Task
    {
        [Key]
        public int Task_ID { get; set; }

        public int? Parent_ID { get; set; }

        public string TaskName { get; set; }

        public DateTime? Start_Date { get; set; }

        public DateTime? End_Date { get; set; }

        public int Priority { get; set; }

        public string Status { get; set; }

        public virtual ParentTask ParentTask { get; set; }
    }
}
