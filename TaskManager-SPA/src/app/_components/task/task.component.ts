import { Component, OnInit, NgProbeToken, TemplateRef } from '@angular/core';
import { Form, FormGroup } from '@angular/forms';
import { Router,ActivatedRoute,ParamMap } from '@angular/router';
import { TaskService } from 'src/app/_services/task.service';
import { TaskData } from 'src/app/_models/task.data';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ParentTaskData } from 'src/app/_models/parenttask.data';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  private title: string = 'Add Task';
  private  SubmitBtnName: string = 'Add Task';
  private parenttask: string;
  private parentid: number;
  private showdate: boolean = false;
  private isChecked:boolean;
  private todate = new Date();
  tasks: TaskData[]=[];
  parenttasks: ParentTaskData[]; 
  isEdit: boolean;
  taskId: number;
  taskData: TaskData = { Task_ID: null, Parent_ID: null,TaskName: '', Start_Date:  new Date(), End_Date: this.todate, Priority: 0, Status: '', ParentTask:null}
  parenttaskData: ParentTaskData = { Parent_ID: null, Parent_Task: '' }

  constructor(private modalService: BsModalService, private router: Router, private taskService: TaskService,
   private route: ActivatedRoute) { } // {2}
  ngOnInit() {
      let id = this.route.snapshot.paramMap.get('id');
      if (id != null){
      this.edit(parseInt(id));
    
    }
 
    this.taskService.getparentTasks()
      .subscribe(response => {
        this.parenttasks = response;
        console.log(response);
      })
   
    this.todate.setDate( this.todate.getDate() + 1 );
  }
  public modalRef: BsModalRef; // {1}

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);// {3}
  }

  public closeModal(template: TemplateRef<any>) {
    // this.userBodyTextname=template.elementRef.
    this.modalService.hide(1);
    //search(); 
  }
  
  ontaskChange(event: any) {
    //console.log(event);
     this.parenttask = event.value[0].Parent_Task;
     this.parentid = event.value[0].Parent_ID;
    // console.log(Parent_IDtext);Parent_ID

    if (this.parenttask != '')
      this.showdate = true;

  }
  submit(form: FormGroup) {
    const request = form.value;
    if (this.parentid!=null){
    request.Parent_ID = this.parentid.toString();
    }
    if (!this.isEdit) {
      this.taskService.addTask(request)
        .subscribe(response => {
          this.tasks.push(response);
          confirm('Task Details Saved Successfully');
          
        }, error => {
          for (const key of Object.keys(error.error.ModelState)) {
            for (const err of error.error.ModelState[key]) {
              alert(err);
            }
          }
        }, () => {
          form.reset();

        })
    } else {
      request.Task_ID = this.taskId;
      this.taskService.updateTask(this.taskId, request)
        .subscribe(task => {
          var tskIndex = this.tasks.findIndex(tsk => tsk.Task_ID == this.taskId);

          if (tskIndex != -1) {
            this.tasks[tskIndex] = task;
          }
          confirm('Task Details Updated Successfully');
          this.router.navigate(['/viewtask']);

        }, error => {
          for (const key of Object.keys(error.error.ModelState)) {
            for (const err of error.error.ModelState[key]) {
              alert(err);
            }
          }
        }, () => {
          form.reset();
          this.isEdit = false;
        })
    }
  }

  delete(id: number) {
    let isDelete = confirm('Do you wish to delete the Task?')

    if (isDelete) {
      this.taskService.deleteTask(id)
        .subscribe(response => {
          if (response) {
            const projIndex = this.tasks.findIndex(proj => proj.Task_ID == id);
            if (projIndex != -1) {
              this.tasks.splice(projIndex, 1);
            }
          }
        });
    }
  }

  edit(id: number) {
this.title = 'Update Task';
  this.SubmitBtnName = 'Update';
    this.taskService.getTask(id)
      .subscribe(task => {
        this.taskData.Parent_ID = task.Parent_ID;
        this.taskData.TaskName = task.TaskName;
        this.taskData.End_Date = task.End_Date;
        this.taskData.Start_Date = task.Start_Date;
        this.taskData.Priority = task.Priority;
        this.taskData.ParentTask = task.ParentTask;

        this.parenttask = task.ParentTask.Parent_Task;
     this.parentid = task.ParentTask.Parent_ID;

        this.isEdit = true;
        this.taskId = id;
      })
  }

  view(id: number) {
    this.router.navigate(['/tasks', id]);
  }

  config = {
            displayKey:"Parent_Task" ,//if objects array passed which key to be displayed defaults to description,
            search:true//enables the search plugin to search in the list
          }
          
    singleSelect: any = [];      

}
