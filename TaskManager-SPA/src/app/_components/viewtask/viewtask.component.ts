import { Component, OnInit, NgProbeToken, TemplateRef } from '@angular/core';
import { Form, FormGroup } from '@angular/forms';
import { Router,ParamMap } from '@angular/router';
import { TaskService } from 'src/app/_services/task.service';
import { TaskData } from 'src/app/_models/task.data';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ParentTaskData } from 'src/app/_models/parenttask.data';
import { Location } from '@angular/common';


@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css']
})
export class ViewtaskComponent implements OnInit {
  private parenttask: string;
  private parentid: number;

  fTask: string ="";
  fParentTask: string="";
  fPriotityFrom: number=0;
  fPriorityTo: number=99999999;
  fStartDate: Date=null;
  fEndDate: Date=null;
  tasks: TaskData[] = [];
  filteredTasks: TaskData[] = [];
  parenttasks: ParentTaskData[]; 
  isEdit: boolean; 
  taskId: number;
  taskData: TaskData = { Task_ID: null, Parent_ID: null, TaskName: '', Start_Date: null, End_Date: null, Priority: null,Status:'', ParentTask:null }
  parenttaskData: ParentTaskData = { Parent_ID: null, Parent_Task: '' }

  constructor(private modalService: BsModalService,
    private router: Router, 
    private taskService: TaskService,
    private location: Location) { } // {2}
  ngOnInit() {
this.taskService.getparentTasks() 
      .subscribe(response => {
        this.parenttasks = response;
        console.log(response);
      })
    this.taskService.getTasks()
      .subscribe(response => {
        this.tasks = response;
        this.filteredTasks = this.tasks;
        console.log(response);
      });
  }
  public modalRef: BsModalRef; // {1}

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);// {3}
  }

  public closeModal(template: TemplateRef<any>) {
    // this.userBodyTextname=template.elementRef.
    this.modalService.hide(1);
    //search();
  }

  updateFilter()
  {
    this.filteredTasks = this.tasks.filter(x =>
       (x.TaskName == '' || x.TaskName.indexOf(this.fTask) > -1)
      && 
      ((x.ParentTask.Parent_Task == '') 
       || (x.ParentTask.Parent_Task.indexOf(this.fParentTask) > -1))
      && 
      (x.Priority >= this.fPriotityFrom)
      &&
      (x.Priority <= this.fPriorityTo)
      &&
      (this.fStartDate == null || x.Start_Date >= this.fStartDate)
      &&
      (this.fEndDate==null || x.End_Date <= this.fEndDate)
    );
  }
  onTaskChange(searchValue : string ) {  
    this.fTask = searchValue;
    this.updateFilter();
  }
  onPriorityFromChange(searchValue : number ) {  
    this.fPriotityFrom = searchValue;
    this.updateFilter();
  }
  onPriorityToChange(searchValue : number ) {  
    this.fPriorityTo = searchValue;
    this.updateFilter();
  }
  onParentTaskChange(searchValue : string ) { 
    this.fParentTask = searchValue;
    this.updateFilter();
  }
  onStartDateChange(searchValue : Date ) {  
    this.fStartDate = searchValue;
    this.updateFilter();
  }
  onEndDateChange(searchValue : Date ) {  
    this.fEndDate = searchValue;
    this.updateFilter();
  }
  submit(form: FormGroup) {
    const request = form.value;
    request.Parent_ID = this.parentid.toString();
    if (!this.isEdit) {
      this.taskService.addTask(request)
        .subscribe(response => {
          this.tasks.push(response);
          console.log(response);
        }, error => {
          for (const key of Object.keys(error.error.ModelState)) {
            for (const err of error.error.ModelState[key]) {
              alert(err);
            }
          }
        }, () => {
          form.reset();

        })
    } else {
      this.taskService.updateTask(this.taskId, request)
        .subscribe(task => {
          var tskIndex = this.tasks.findIndex(tsk => tsk.Task_ID == this.taskId);

          if (tskIndex != -1) {
            this.tasks[tskIndex] = task;
          }
          confirm('Task Details Updated Successfully');
        }, error => {
          for (const key of Object.keys(error.error.ModelState)) {
            for (const err of error.error.ModelState[key]) {
              alert(err);
            }
          }
        }, () => {
          form.reset();
          this.isEdit = false;
        })
    }
  }

  delete(id: number) {
    let isDelete = confirm('Do you wish to End the Task?')

    if (isDelete) {
      this.taskService.deleteTask(id)
        .subscribe(task => {
          var tskIndex = this.tasks.findIndex(tsk => tsk.Task_ID == this.taskId);

          if (tskIndex != -1) {
            this.tasks[tskIndex] = task;
          }
          confirm('Task has been Completed Successfully');
          location.reload();
        }, error => {
          for (const key of Object.keys(error.error.ModelState)) {
            for (const err of error.error.ModelState[key]) {
              alert(err);
            }
          }
        }, () => { 
         // form.reset();
          this.isEdit = false;
        })
    }
  }

  edit(id: number) {
    this.taskService.getTask(id)
      .subscribe(task => {
        this.taskData.Parent_ID = task.Parent_ID;
        this.taskData.TaskName = task.TaskName;
        this.taskData.End_Date = task.End_Date;
        this.taskData.Start_Date = task.Start_Date;
        this.taskData.Priority = task.Priority;
        this.taskData.ParentTask = task.ParentTask;
        this.isEdit = true;
        this.taskId = id;
      })
  }

view(id: number) {
    this.router.navigate(['/tasks', id]);
  }
  sort(param: any) {
    if (param == 'StartDate') {
      this.taskService.getTasks()
        .subscribe(response => {
          this.tasks = response.sort((a, b) => {
            return <any>new Date(b.Start_Date) - <any>new Date(a.Start_Date);
          });
        })
    }
    if (param == 'EndDate') {
      this.taskService.getTasks()
        .subscribe(response => {
          this.tasks = response.sort((a, b) => {
            return <any>new Date(b.End_Date) - <any>new Date(a.End_Date);
          });
        })
    }
    if (param == 'Priority') {
      this.taskService.getTasks()
        .subscribe(response => {
          this.tasks = response.sort((a, b) => a.Priority.toString().localeCompare(b.Priority.toString(), undefined, { numeric: true }));
        });
    }
  }
  
}
