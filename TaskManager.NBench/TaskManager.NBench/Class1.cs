﻿using NBench;
using TaskManager.Controllers.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.NBench
{
    public class TaskManagerPerfSpecs
    {
        private Counter _counter;

        [PerfSetup]
        public void Setup(BenchmarkContext context)
        {
            _counter = context.GetCounter("TestCounter");
        }

        
        [PerfBenchmark(Description = "Test to ensure the performance parameters of Get All Tasks API.",
            NumberOfIterations = 10, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 1000, TestMode = TestMode.Test)]
        [CounterMeasurement("TestCounter")]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.GreaterThanOrEqualTo, ByteConstants.ThirtyTwoKb)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.GreaterThanOrEqualTo, 0.0d)]
        public void BenchmarkGetAllTasks()
        {
            var TaskTests = new TasksControllerTests();
            TaskTests.GetTasks_All_ReturnAllTasks();

            _counter.Increment();
        }

        [PerfBenchmark(Description = "Test to ensure the performance parameters of Add new task API.",
            NumberOfIterations = 10, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 1000, TestMode = TestMode.Test)]
        [CounterMeasurement("TestCounter")]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.GreaterThanOrEqualTo, ByteConstants.ThirtyTwoKb)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.GreaterThanOrEqualTo, 0.0d)]
        public void BenchmarkAddTasks()
        {
            var TAskTests = new TasksControllerTests();
            TAskTests.AddNewTask_ValidTask_ReturnsAllTasks();

            _counter.Increment();
        }

        [PerfCleanup]
        public void Cleanup()
        {

        }
    }
}
